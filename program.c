#include <stdio.h>
#include <stdlib.h> // exit(), strtol()

void sort(long int *, int);

FILE * fptr = NULL;
FILE * fptr2 = NULL;

int main(){
	int i;
    long int N;
    long int n[3];
	char buff[32];

    fptr = fopen("list", "r"); // opening file
    if(fptr == 0){
        printf("Error - the file could not be opened.\n");
        exit(1);
    }else{
        fscanf(fptr, "%s", buff); // reading from file
        N = strtol(buff, NULL, 10); // conversion string to long int
        for(int i = 0;  i < 3; i++ ){
            fscanf(fptr, "%s", buff);
            n[i] = strtol(buff, NULL, 10);
        }
    }
	fclose(fptr); // closing file

    int n_size = sizeof(n) / sizeof(*n);
    sort(n, n_size);

    fptr2 = fopen("list_sorted", "w"); // opening file
	fprintf(fptr2, "%ld ", N); // writing to file
	for(int i = 0;  i < 3; i++ ){
        fprintf(fptr2, "%ld ", n[i]);
    }
	fclose(fptr2); // closing file

	return 0;
}

//////////////////////////////////////////////////
void sort(long int * n, int n_size)
{
    int outer, inner;
    long int temp;

    for(outer=0; outer<2; outer++){
        for(inner=outer; inner<3; inner++){
            if(n[outer] > n[inner]){ //sorts in ascending order
                temp = n[outer];
                n[outer] = n[inner]; 
                n[inner] = temp;
            }
        }
    }
}